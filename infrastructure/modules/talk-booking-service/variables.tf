variable "region" {
  description = "The AWS region to create resources in."
}

variable "vpc_id" {
  description = "ID od VPC"
  type = string
}

variable "environment_name" {
  description = "Name of app environment. Must be unique."
  type = string
}

variable "eks_security_group_id" {
  description = "ID of EKS security group"
  type = string
}

variable "load_balancer_security_group_id" {
  description = "ID of ALB security group"
  type = string
}

variable "log_retention_in_days" {
  description = "Log retention in days"
  type = number
}

variable "public_subnet_1_id" {
  description = "Id of first public subnet"
  type = string
}

variable "public_subnet_2_id" {
  description = "Id of second public subnet"
  type = string
}

variable "private_subnet_1_id" {
  description = "Id of first private subnet"
  type = string
}

variable "private_subnet_2_id" {
  description = "Id of second private subnet"
  type = string
}

variable "app_environment" {
  description = "Application environment"
  type = string
}

variable "node_group_desired_capacity" {
  description = "Desired number of instances in the EKS node group"
  type        = number
}

variable "node_group_min_capacity" {
  description = "Minimum number of instances in the EKS node group"
  type        = number
}

variable "node_group_max_capacity" {
  description = "Maximum number of instances in the EKS node group"
  type        = number
}

