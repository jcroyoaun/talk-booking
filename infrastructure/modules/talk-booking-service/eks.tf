resource "aws_eks_cluster" "talk_booking_cluster" {
  name     = "${var.environment_name}-cluster"
  role_arn = aws_iam_role.eks_cluster_role.arn

  vpc_config {
    subnet_ids = [var.public_subnet_1_id, var.public_subnet_2_id, var.private_subnet_1_id, var.private_subnet_2_id]
  }

  depends_on = [aws_security_group_rule.cluster]
}

resource "aws_eks_node_group" "talk_booking_node_group" {
  cluster_name    = aws_eks_cluster.talk_booking_cluster.name
  node_group_name = "${var.environment_name}-node-group"
  node_role_arn   = aws_iam_role.eks_node_role.arn
  # Updated to use existing subnet variables
  subnet_ids      = [var.private_subnet_1_id, var.private_subnet_2_id]

  scaling_config {
    desired_size = var.node_group_desired_capacity
    min_size     = var.node_group_min_capacity
    max_size     = var.node_group_max_capacity
  }

  depends_on = [aws_security_group_rule.cluster]
}


resource "aws_security_group" "worker_group_mgmt_one" {
  name_prefix = "worker_group_mgmt_one"
  description = "worker_group_mgmt_one"
  vpc_id      = var.vpc_id
}

resource "aws_security_group" "worker_group_mgmt_two" {
  name_prefix = "worker_group_mgmt_two"
  description = "worker_group_mgmt_two"
  vpc_id      = var.vpc_id
}

resource "aws_security_group" "worker_group" {
  name_prefix = "worker_group"
  description = "worker_group"
  vpc_id      = var.vpc_id
}

resource "aws_security_group_rule" "cluster" {
  security_group_id = aws_security_group.worker_group_mgmt_one.id
  type        = "ingress"
  from_port   = 0
  to_port     = 65535
  protocol    = "tcp"
  cidr_blocks = ["10.0.0.0/8"]
}
